import 'package:auth_n_list/features/login/data/repositories/mock_auth.dart';
import 'package:auth_n_list/features/login/presentation/state/login_store.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'features/login/presentation/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Teste Target',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Provider(
        create: (_) => LoginStore(MockAuth()),
        child: const LoginPage(),
      ),
    );
  }
}
