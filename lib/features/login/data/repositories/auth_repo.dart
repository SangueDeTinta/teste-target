abstract class AuthRepo {
  Future<bool> login(String user, String password);
}
