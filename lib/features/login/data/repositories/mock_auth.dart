import 'package:auth_n_list/features/login/data/repositories/auth_repo.dart';

class MockAuth implements AuthRepo {
  @override
  Future<bool> login(String user, String password) async {
    final response = Future.delayed(const Duration(seconds: 2), () {
      if (user == 'error' && password == 'error') {
        throw MockNetworkError();
      }
      return true;
    });
    return response;
  }
}

class MockNetworkError extends Error {}
