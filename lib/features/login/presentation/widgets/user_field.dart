import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UserField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final Key fieldKey;
  const UserField({
    Key? key,
    required this.controller,
    required this.focusNode,
    required this.fieldKey,
  }) : super(key: key);

  static const labelStyle = TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w300,
  );
  static const fieldStyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontSize: 18,
  );
  static const errorStyle = TextStyle(
    color: Colors.orange,
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Text('Usuário', style: labelStyle),
          ),
          TextFormField(
            key: fieldKey,
            controller: controller,
            focusNode: focusNode,
            style: fieldStyle,
            textAlignVertical: TextAlignVertical.center,
            maxLength: 20,
            validator: validate,
            cursorColor: Colors.black,
            cursorWidth: 2,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(4),
              ),
              fillColor: Colors.white,
              filled: true,
              prefixIcon: const Icon(FontAwesomeIcons.solidUser, size: 18),
              contentPadding: EdgeInsets.zero,
              counterText: '',
              errorStyle: errorStyle,
            ),
          ),
        ],
      ),
    );
  }

  String? validate(String? value) {
    RegExp regex = RegExp(r'^(?!\s)[\s\S]{1,20}(?<!\s)$');

    return value == null || value.isEmpty
        ? 'Por favor insira um usuario'
        : value.isNotEmpty && !regex.hasMatch(value)
            ? 'Por favor insira um usuario valido'
            : null;
  }
}
