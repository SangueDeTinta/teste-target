import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PolicyButton extends StatelessWidget {
  const PolicyButton({Key? key}) : super(key: key);

  static const textStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w300,
    fontSize: 16,
  );

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: _launchUrl,
      child: const Text(
        'Política de Privacidade',
        style: textStyle,
      ),
    );
  }

  Future<void> _launchUrl() async {
    final url = Uri.parse("https://www.google.com.br");

    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }
}
