import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final Function() loginCallback;

  const LoginButton({required this.loginCallback, Key? key}) : super(key: key);

  static const textStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w400,
    fontSize: 18,
  );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 150,
      child: ElevatedButton(
        onPressed: loginCallback,
        style: ElevatedButton.styleFrom(
          elevation: 1,
          backgroundColor: const Color.fromARGB(255, 68, 189, 110),
        ),
        child: const Text('Entrar', style: textStyle),
      ),
    );
  }
}
