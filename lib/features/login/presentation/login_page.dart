import 'package:auth_n_list/core/enums/enums.dart';
import 'package:auth_n_list/features/home/presentation/home_page.dart';
import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:auth_n_list/features/login/presentation/state/login_store.dart';
import 'package:auth_n_list/features/login/presentation/widgets/login_button.dart';
import 'package:auth_n_list/features/login/presentation/widgets/password_field.dart';
import 'package:auth_n_list/features/login/presentation/widgets/policy_button.dart';
import 'package:auth_n_list/features/login/presentation/widgets/user_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final userController = TextEditingController();
  final userFocus = FocusNode();
  final userFieldKey = GlobalKey<FormFieldState>();
  final passwordController = TextEditingController();
  final passwordFocus = FocusNode();
  final passwordFieldKey = GlobalKey<FormFieldState>();
  LoginStore? _loginStore;
  List<ReactionDisposer>? _disposers;

  static const List<Color> gradientColors = [
    Color.fromARGB(255, 31, 84, 102),
    Color.fromARGB(255, 45, 149, 142),
  ];

  @override
  void initState() {
    super.initState();
    setFocusListeners();
  }

  @override
  void dispose() {
    userController.dispose();
    userFocus.dispose();
    passwordController.dispose();
    passwordFocus.dispose();
    _disposers?.forEach((d) => d());
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _loginStore ??= Provider.of<LoginStore>(context);
    _disposers ??= [
      reaction(
        // Tell the reaction which observable to observe
        (_) => _loginStore?.isLogged,
        // Run some logic with the content of the observed field
        (bool? navigate) {
          if (navigate == true) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (_) => Provider(
                create: (_) => HomeStore(),
                child: const HomePage(),
              ),
            ));
          }
        },
      ),
      reaction(
        // Tell the reaction which observable to observe
        (_) => _loginStore?.errorMessage,
        // Run some logic with the content of the observed field
        (String? message) {
          if (message != null) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('Errou feio, errou rude!'),
              ),
            );
          }
        },
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final store = Provider.of<LoginStore>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: size.height,
        width: size.width,
        padding: const EdgeInsets.symmetric(horizontal: 36),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: gradientColors,
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Observer(
          builder: (context) {
            if (store.state == StoreState.initial ||
                store.state == StoreState.loaded) {
              return _buildInitialContent(size);
            } else {
              return _buildLoadingContent(size);
            }
          },
        ),
      ),
    );
  }

  Widget _buildInitialContent(Size size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                UserField(
                  controller: userController,
                  focusNode: userFocus,
                  fieldKey: userFieldKey,
                ),
                const SizedBox(height: 10),
                PasswordField(
                  controller: passwordController,
                  focusNode: passwordFocus,
                  fieldKey: passwordFieldKey,
                ),
                const SizedBox(height: 20),
                Align(
                  alignment: Alignment.center,
                  child: LoginButton(
                    loginCallback: () => login(
                      userController.text,
                      passwordController.text,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        const PolicyButton(),
      ],
    );
  }

  Widget _buildLoadingContent(Size size) {
    return const Center(
      child: CircularProgressIndicator(
        color: Colors.white,
        strokeWidth: 6,
      ),
    );
  }

  void setFocusListeners() {
    userFocus.addListener(() {
      if (!userFocus.hasFocus) {
        userFieldKey.currentState?.validate();
      }
    });
    passwordFocus.addListener(() {
      if (!passwordFocus.hasFocus) {
        passwordFieldKey.currentState?.validate();
      }
    });
  }

  Future<void> login(String user, String password) async {
    if (_formKey.currentState!.validate()) {
      await Provider.of<LoginStore>(context, listen: false)
          .login(user, password);
      userController.clear();
      passwordController.clear();
    }
  }
}
