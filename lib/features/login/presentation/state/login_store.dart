import 'package:auth_n_list/core/enums/enums.dart';
import 'package:auth_n_list/features/login/data/repositories/auth_repo.dart';
import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore extends _LoginStore with _$LoginStore {
  LoginStore(AuthRepo authRepo) : super(authRepo);
}

abstract class _LoginStore with Store {
  final AuthRepo _authRepo;

  _LoginStore(this._authRepo);

  @observable
  ObservableFuture<bool>? _authFuture;

  @observable
  bool isLogged = false;

  @observable
  String? errorMessage;

  @computed
  StoreState get state {
    if (_authFuture == null || _authFuture!.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _authFuture!.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  Future<void> login(String user, String password) async {
    try {
      errorMessage = null;
      _authFuture = ObservableFuture(_authRepo.login(user, password));
      isLogged = await _authFuture!;
    } catch (e) {
      errorMessage = e.toString();
    }
  }
}
