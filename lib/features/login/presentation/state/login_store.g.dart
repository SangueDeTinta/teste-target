// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$LoginStore on _LoginStore, Store {
  Computed<StoreState>? _$stateComputed;

  @override
  StoreState get state => (_$stateComputed ??=
          Computed<StoreState>(() => super.state, name: '_LoginStore.state'))
      .value;

  late final _$_authFutureAtom =
      Atom(name: '_LoginStore._authFuture', context: context);

  @override
  ObservableFuture<bool>? get _authFuture {
    _$_authFutureAtom.reportRead();
    return super._authFuture;
  }

  @override
  set _authFuture(ObservableFuture<bool>? value) {
    _$_authFutureAtom.reportWrite(value, super._authFuture, () {
      super._authFuture = value;
    });
  }

  late final _$isLoggedAtom =
      Atom(name: '_LoginStore.isLogged', context: context);

  @override
  bool get isLogged {
    _$isLoggedAtom.reportRead();
    return super.isLogged;
  }

  @override
  set isLogged(bool value) {
    _$isLoggedAtom.reportWrite(value, super.isLogged, () {
      super.isLogged = value;
    });
  }

  late final _$errorMessageAtom =
      Atom(name: '_LoginStore.errorMessage', context: context);

  @override
  String? get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String? value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  late final _$loginAsyncAction =
      AsyncAction('_LoginStore.login', context: context);

  @override
  Future<void> login(String user, String password) {
    return _$loginAsyncAction.run(() => super.login(user, password));
  }

  @override
  String toString() {
    return '''
isLogged: ${isLogged},
errorMessage: ${errorMessage},
state: ${state}
    ''';
  }
}
