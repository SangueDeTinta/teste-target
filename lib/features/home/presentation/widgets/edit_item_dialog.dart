import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:flutter/material.dart';

class EditItemDialog extends StatelessWidget {
  final int index;
  final HomeStore store;
  final TextEditingController controller;

  const EditItemDialog({
    Key? key,
    required this.index,
    required this.store,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.text = store.notes[index];

    return AlertDialog(
      content: SizedBox(
        height: 100,
        width: double.infinity,
        child: TextField(
          controller: controller,
          minLines: 4,
          maxLines: 5,
        ),
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Cancelar'),
        ),
        TextButton(
          onPressed: () {
            store.updateItem(index, controller.text);
            Navigator.pop(context);
          },
          child: const Text('Confirmar'),
        ),
      ],
    );
  }
}
