import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:flutter/material.dart';

class ConfirmDialog extends StatelessWidget {
  final int index;
  final HomeStore store;

  const ConfirmDialog({Key? key, required this.index, required this.store})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: const SizedBox(
        height: 50,
        width: double.infinity,
        child: Center(
          child: Text('Tem certeza que deseja excluir esse item?'),
        ),
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Cancelar'),
        ),
        TextButton(
          onPressed: () {
            store.deleteItem(index);
            Navigator.pop(context);
          },
          child: const Text('Confirmar'),
        ),
      ],
    );
  }
}
