import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:flutter/material.dart';

import 'confirm_dialog.dart';

class DeleteItemButton extends StatelessWidget {
  final int index;
  final HomeStore store;
  const DeleteItemButton({Key? key, required this.index, required this.store})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) {
            return ConfirmDialog(store: store, index: index);
          },
        );
      },
      icon: const Stack(
        alignment: Alignment.center,
        children: [
          Icon(
            Icons.circle,
            color: Colors.red,
            size: 50,
          ),
          Icon(
            Icons.close,
            color: Colors.white,
            size: 40,
          ),
        ],
      ),
    );
  }
}
