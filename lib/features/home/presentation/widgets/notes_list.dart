import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

import 'note_tile.dart';

class NotesList extends StatelessWidget {
  const NotesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final store = Provider.of<HomeStore>(context);
    return Container(
      height: 360,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.elliptical(5, 50),
        ),
      ),
      child: Observer(
        builder: (context) {
          return ListView.builder(
            padding: const EdgeInsets.symmetric(
              horizontal: 8,
              vertical: 0,
            ),
            itemCount: store.notes.length,
            itemBuilder: (context, index) {
              return NoteTile(
                noteString: store.notes[index],
                index: index,
                store: store,
              );
            },
          );
        },
      ),
    );
  }
}
