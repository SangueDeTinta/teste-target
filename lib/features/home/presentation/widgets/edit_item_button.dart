import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:flutter/material.dart';

import 'edit_item_dialog.dart';

class EditItemButton extends StatefulWidget {
  final int index;
  final HomeStore store;

  const EditItemButton({Key? key, required this.index, required this.store})
      : super(key: key);

  @override
  State<EditItemButton> createState() => _EditItemButtonState();
}

class _EditItemButtonState extends State<EditItemButton> {
  final TextEditingController dialogController = TextEditingController();

  @override
  void dispose() {
    dialogController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            style: BorderStyle.solid,
            width: 1,
            color: Colors.black,
          ),
        ),
      ),
      child: IconButton(
        padding: EdgeInsets.zero,
        onPressed: () => showDialog(
          context: context,
          builder: (context) {
            return EditItemDialog(
              index: widget.index,
              store: widget.store,
              controller: dialogController,
            );
          },
        ),
        icon: const Icon(
          Icons.border_color,
          size: 40,
        ),
      ),
    );
  }
}
