import 'package:auth_n_list/features/home/presentation/state/home_store.dart';
import 'package:auth_n_list/features/home/presentation/widgets/delete_item_button.dart';
import 'package:auth_n_list/features/home/presentation/widgets/edit_item_button.dart';
import 'package:flutter/material.dart';

class NoteTile extends StatelessWidget {
  final String noteString;
  final HomeStore store;
  final int index;

  const NoteTile(
      {Key? key,
      required this.noteString,
      required this.index,
      required this.store})
      : super(key: key);

  static const textStyle = TextStyle(
    fontSize: 18,
    color: Colors.black,
    fontWeight: FontWeight.w800,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 50, maxHeight: 70),
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 8),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Colors.grey,
            style: BorderStyle.solid,
          ),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: Text(
              noteString,
              textAlign: TextAlign.center,
              style: textStyle,
            ),
          ),
          EditItemButton(index: index, store: store),
          DeleteItemButton(index: index, store: store),
        ],
      ),
    );
  }
}
