import 'package:auth_n_list/core/enums/enums.dart';
import 'package:mobx/mobx.dart';

part 'home_store.g.dart';

class HomeStore extends _HomeStore with _$HomeStore {}

abstract class _HomeStore with Store {
  @observable
  ObservableFuture<List<String>>? _notesFuture;

  @observable
  ObservableList<String> notes = ObservableList.of([
    'teste 1',
    'teste 2',
    'teste 3',
  ]);

  @observable
  String? errorMessage;

  @computed
  StoreState get state {
    if (_notesFuture == null || _notesFuture!.status == FutureStatus.rejected) {
      return StoreState.initial;
    }
    return _notesFuture!.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  void addNewItem(String newItem) => notes.add(newItem);

  @action
  void updateItem(int index, String newContent) => notes[index] = newContent;

  @action
  void deleteItem(int index) => notes.removeAt(index);
}
